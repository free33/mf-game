<?php 
ob_start();
session_start();
require_once 'config.php'; 
if(!isset($_SESSION['logged_in'])){
	header('Location: /');
	exit;
}
		try {
			$user = new Cl_User();
			$results = $user->startGame();
		} catch (Exception $e) {
			$_SESSION['error'] = $e->getMessage();
			header('Location: /');exit;

		} 
		
?>
<?php require_once 'templates/header.php';?>
<div class="content bg-pic2 pt-10">
     	<div class="game-container">
			<?php require_once 'templates/message.php';?>
			<div class="quiz-wrapper">
				<div class="quiz-card text-center">
					<span id="timer"></span>
					<form class="" role="form" id='quiz_form' method="post" action="quiz-result" style="padding-top: 20px;">
						<?php
						$rows =  $results['rowcount'];
						$answers = $_SESSION['is_correct'];
						$k = 0;
						?>
						<?php foreach ($results['questions'] as $quiz) { ?>
							<?php if ($k >= count($answers)) { ?>
							<div class="cont" id="<?php echo 'question-'.$k;?>">
								<p class='question-title'><?php echo $quiz['question_name'];?></p>
																<?php
								if ( $k == $rows-1 ) { ?>
									<button name="<?php echo $k;?>" value="1" id="<?php echo $k.'_1';?>" class='answ btn btn-answ' type='submit'><?php echo $quiz['answer1'];?></button>
									<button name="<?php echo $k;?>" value="2" id="<?php echo $k.'_2';?>" class='answ btn btn-answ' type='submit'><?php echo $quiz['answer2'];?></button>
									<button name="<?php echo $k;?>" value="3" id="<?php echo $k.'_3';?>" class='answ btn btn-answ' type='submit'><?php echo $quiz['answer3'];?></button>
									<button name="<?php echo $k;?>" value="4" id="<?php echo $k.'_4';?>" class='answ btn btn-answ' type='submit'><?php echo $quiz['answer4'];?></button>
									<button name="<?php echo $k;?>" value="5" id="<?php echo $k.'_5';?>" class='answ hide' type='submit'></button>
									<input type="radio" checked="checked" style="display:none" value="5" name="<?php echo $k;?>"/>     

								
								<?php } else { ?>
									<button name="<?php echo $k;?>" value="1" id="<?php echo $k.'_1';?>" class='answ btn btn-answ' type='button'><?php echo $quiz['answer1'];?></button>
									<button name="<?php echo $k;?>" value="2" id="<?php echo $k.'_2';?>" class='answ btn btn-answ' type='button'><?php echo $quiz['answer2'];?></button>
									<button name="<?php echo $k;?>" value="3" id="<?php echo $k.'_3';?>" class='answ btn btn-answ' type='button'><?php echo $quiz['answer3'];?></button>
									<button name="<?php echo $k;?>" value="4" id="<?php echo $k.'_4';?>" class='answ btn btn-answ' type='button'><?php echo $quiz['answer4'];?></button>
									<button name="<?php echo $k;?>" value="5" id="<?php echo $k.'_5';?>" class='answ hide' type='button'></button>
									<input type="radio" checked="checked" style="display:none" value="5" name="<?php echo $k;?>"/>     
								<?php } ?>
								
							</div>
						<?php }
							elseif ($answers[$k]){?>
								<input type="radio" checked="checked" style="display:none" value="<?php echo $_SESSION['answers'][$k] ?>" name="<?php echo $k;?>">     
							<?php }?>	
						<?php $k++; ?>
						<?php } ?>	
						</form>
				</div>
				<div class="quiz-pagination">
				<?php foreach ($answers as  $value) 
				{
						if ($value) echo '<span class="correct"></span>';
						else echo '<span class="wrong"></span>';
						
					}?>
				 <?php for ($i = 1; $i <= $rows-count($answers); $i++){
					 echo "<span></span>";
				 } ?>
				</div>
			</div>
		</div>	
</div>
<script>
var number = <?php echo count($answers);?>;
var questions = <?php echo $results['options']['num_questions'];?>;
var seconds = <?php echo $results['options']['time_question_phone'];?>;
var initial = seconds*100;
var count = initial;
var counter;
var stopwatch = 0;

$('.cont').hide();
$("#question-"+number).show();

displayCount(initial);
counter = setInterval(timer, 10);

$('.answ').on('click',function(){
	number=parseInt($(this).attr('name'));
	value = $(this).val();
	$("input[name = '"+number+"']").val(value);
	$("button[name = '"+number+"']").prop('disabled', true);
	var answerButton = $(this);


	$.ajax({
		url: './server.php', type: 'POST', dataType: 'json', 
		data: {
			question: number, 
			answer: value, 
			time : stopwatch
		},
		cache: false,
		headers: {
              "cache-control": "no-cache"
            },
		success: function(iscorrect){
			if (iscorrect){
				$(".quiz-pagination span:nth-child("+(number+1)+")").addClass('correct');  
				answerButton.addClass('correct');
			} else{
				$(".quiz-pagination span:nth-child("+(number+1)+")").addClass('wrong');  
				answerButton.addClass('wrong');
			}
		}
	});
	clearInterval(counter);
	
	setTimeout(function(){nextQuestion()}, 500);
	
	
});

function nextQuestion() {
	if ((number+1) < questions) {
	$("#question-"+number).hide();
	number++;
	$("#question-"+number).show();
	
	count = initial;
	displayCount(count);
	counter = setInterval(timer, 10);
	}
	else {$('#quiz_form').submit()}
}


function timer() {
	if (count <= 0) {
		clearInterval(counter);
		$('#'+number+'_5').click();
		return;
	}
	count--;
	displayCount(count);
}

function displayCount(count) {
	var res = count / 100;
	stopwatch = ((seconds-res).toFixed(2));
	var min = parseInt( res / 60 );
	var sec = parseInt( res % 60 );

	result = (min < 10 ? "0" + min : min) + ":" + (sec  < 10 ? "0" + sec : sec);
	$('#timer').html(result);
}		
</script>
<?php require_once 'templates/footer.php';?>   