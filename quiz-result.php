<?php 
ob_start();
session_start();
require_once 'config.php'; 
if(!isset($_SESSION['logged_in'])){
	header('Location: /');
	exit;	
}
	if( !empty( $_POST )){
		try {
			$user = new Cl_User();
			$result = $user->endGame( $_POST );
		} catch (Exception $e) {
			$_SESSION['is_correct'] = array();
			$_SESSION['error'] = $e->getMessage();
			header('Location: /');exit;

		} 
	}else{
		header('Location: /');exit;
	}
?>
<?php require_once 'templates/header.php';?>
	<div class="content bg-pic1 pt-50">
		<div class="game-container">
			<a class="logo as-supersign"><img src="img/logo-white.svg"></a>
				<div id="intop" <?php if (!$result['inleaders']){ echo 'class="hide"';}?> >
				<h1 id="title" class="color-white">Ура!</h1>
				<h2 class="color-mfpurple">Ты попал в число лидеров! <br>Но не расслабляйся, соперники не дремлют.</h2>
				<p class="color-white text-small" style="padding-bottom: 20px;">Попробуй пройти тест ещё раз, чтобы улучшить свои позиции!</p>
				<a href="/" class="btn btn-mfpurple btn-login">Начать</a>
				</div>
				<div id="notintop" <?php if ($result['inleaders']){ echo 'class="hide"';}?> >
				<h1 id="title" id="notintop" class="color-white">Упс :(</h1>
				<p class="color-white">Кто-то оказался быстрее и точнее тебя!</p>
				<p class="color-white" style="padding-bottom: 20px;">Попробуй пройти тест ещё раз, пока не закончился таймер на главном экране!</p>
				<a href="/" class="btn btn-mfpurple btn-login">Начать</a>				
				</div> 
				<div id="gameover" class="hide">
				<h1 id="title" id="notintop" class="color-white" style="padding-bottom: 30px;">Игра закончилась</h1>
				<a href="/winners" class="btn btn-mfpurple btn-login">Посмотреть итоги</a>				
				</div> 
     	</div>
    </div>

<script>
poll();
var answer;
function gettop5() {
		$.ajax({
		url: '/server.php', type: 'GET', dataType: 'json', 
		data: { check : 'top5'},
		cache: false,
		headers: {
              "cache-control": "no-cache"
            },
		complete: function(tet) {
			answer =  tet.responseJSON;
			if (answer){
				$('#intop').show();
				$('#notintop').hide();
			}
			else{
				$('#intop').hide();
				$('#notintop').show();
			}
        }
	})
}
function poll() {
	$.ajax({
		url: '/server.php', type: 'GET', dataType: 'json', 
		data: { check : 'status'},
		cache: false,
		headers: {
              "cache-control": "no-cache"
            },
		complete: function(tet) {
			answer =  tet.responseJSON;
			if (answer == 'GAMEOVER'){
				clearInterval(repeater);
				$('#intop').hide();
				$('#notintop').hide();
				$('#gameover').show();
			}else{
				gettop5();
			}
        }
	})
};
repeater = setInterval(poll, 3000);

</script>	
<?php require_once 'templates/footer.php';?>