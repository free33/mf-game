<?php

class Cl_DBclass
{
	/**
	 * @var $con will hold database connection
	 */
	public $con;
	
	/**
	 * This will create Database connection
	 */
	public function __construct()
	{
		$this->con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		mysqli_set_charset ($this->con, 'utf8');
		if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
}

class Cl_User
{
	// Для соединения с БД 
	protected $_con;
	
	// Инициализация  DBclass
	public function __construct()
	{
		$db = new Cl_DBclass();
		$this->_con = $db->con;
	}
	
	public function gameStatus()
	{
		$query = "SELECT * FROM options WHERE name = 'game_start' or name = 'game_end' ";
		$row = mysqli_query($this->_con, $query);
		while ($temp = mysqli_fetch_assoc($row)){
			if ($temp['name'] == 'game_start') $game_start = $temp['value'];
			if ($temp['name'] == 'game_end') $game_end = $temp['value'];
		};
		
		if ($game_end) {
			unset($_SESSION['is_correct']);
			unset($_SESSION['answers']);
			unset($_SESSION['questions']);
			unset($_SESSION['stopwatch']);
			unset($_SESSION['score_id']);
			return 'GAMEOVER';
		}
		elseif (!$game_start) {
			unset($_SESSION['is_correct']);
			unset($_SESSION['answers']);
			unset($_SESSION['questions']);
			unset($_SESSION['stopwatch']);
			unset($_SESSION['score_id']);
			return 'WAIT';
		}	else
		return 'RUN';
	}
	
	public function getOptions(){
		$result = mysqli_query( $this->_con, "select name,value from options");	
		$options = array();
		while ( $row = mysqli_fetch_assoc($result) ) {
			$options[$row['name']] = $row['value'];
		}		
	return $options;
	}	

	// Авторизация
	public function login( array $data )
	{
		if( !empty( $data ) ){
			
			$trimmed_data = array_map('trim', $data);
			
			// Убираем из телефона лишние символы
			$phone = preg_replace("/[^0-9]/", '', $trimmed_data['phone'] );
			if((!$phone) ) {
				throw new Exception( LOGIN_FIELDS_MISSING );
			}
			
			$query = "SELECT id, phone, logged_in FROM users where phone = '$phone' ";
			$result = mysqli_query($this->_con, $query);
			$count = mysqli_num_rows($result);
			
			// Пускаем внутрь, если нет авторизации по этому номеру
			if( $count == 1){
				$_SESSION = mysqli_fetch_assoc($result);
				$_SESSION['logged_in'] = true;
				$user_id = $_SESSION['id'];
				$query = "UPDATE users SET logged_in = '".$_SESSION['logged_in']."' WHERE id = '$user_id'";
				mysqli_query( $this->_con, $query);
				mysqli_close($this->_con);
			}else{
				$query = "INSERT INTO users (id, phone) VALUES (NULL, '$phone')";	
				mysqli_query( $this->_con, $query);
				$_SESSION['id'] = mysqli_insert_id($this->_con);
				$_SESSION['logged_in'] = true;
				$_SESSION['phone'] = $phone;
				$user_id = $_SESSION['id'];
				$query = "UPDATE users SET logged_in = '".$_SESSION['logged_in']."' WHERE id = '$user_id'";
				mysqli_query( $this->_con, $query);
				mysqli_close($this->_con);
			}
		} else{
			throw new Exception( LOGIN_FIELDS_MISSING );
		}
	}
	
	
	/**
	 * This handle sign out process
	 */
	public function logout()
	{
		$user_id = $_SESSION['id'];
		$query = "UPDATE users SET logged_in = NULL WHERE id = '$user_id'";
		mysqli_query( $this->_con, $query);
		mysqli_close($this->_con);

		session_unset();
		session_destroy();
		session_start();
		$_SESSION['success'] = LOGOUT_SUCCESS;
		header('Location: index.php');
	}
	
	public function startGame()
	{
			// Если игра была запущена, то возвращаем те же вопросы
			switch ($this->gameStatus()){
				case 'GAMEOVER' :  throw new Exception( GAMEOVER ); break;
				case 'WAIT' :  throw new Exception( BUYEARLYACCESS ); break;	
			}

			if (!isset ($_SESSION['score_id']) && !($_SESSION['score_id']) ) 
			{ 
				$user_id = $_SESSION['id'];
				$query = "INSERT INTO scores ( user_id,right_answer)VALUES ( '$user_id',0)";
				mysqli_query( $this->_con, $query);
				$_SESSION['score_id'] = mysqli_insert_id($this->_con);
				$_SESSION['stopwatch'] = 0;
				$_SESSION['is_correct'] = array();
			}
			$results = array();
			$results['options'] = $this->getOptions();			
			$order = $results['options']['questions'];
			$result = mysqli_query( $this->_con, "SELECT * FROM questions WHERE ID IN ($order) ORDER BY FIELD(id,$order);");	
			$_SESSION['answers'] = array();		
			$results['rowcount'] =  mysqli_num_rows( $result );
			while ( $row = mysqli_fetch_assoc($result) ) {
				$_SESSION['answers'][] = $row['answer'];		
				$results['questions'][] = $row;
			}
			
			mysqli_close($this->_con);
			return $results;
	}
	
	public function endGame(array $data){
		switch ($this->gameStatus()){
			case 'GAMEOVER' : throw new Exception( GAMEOVER ); break;
			case 'WAIT' :  throw new Exception( BUYEARLYACCESS ); break;	
		}
		if( !empty( $data ) && isset ($_SESSION['score_id']) && ($_SESSION['score_id'])){
			$right_answer = count(array_intersect_assoc($data, $_SESSION['answers']));
			$wrong_answer = count(array_diff_assoc($data, $_SESSION['answers']));
			$time=$_SESSION['stopwatch'];
			$score = $wrong_answer*2+$time;
			$user_id = $_SESSION['id'];
			$score_id = $_SESSION['score_id'];
			
			$results = array();
			$update_query = "update scores set score='$score', right_answer='$right_answer', time='$time' where user_id='$user_id' and id ='$score_id' ";
			mysqli_query( $this->_con, $update_query)   or die(mysqli_error());
			$results['inleaders'] = $this->checkLeaders();
			mysqli_close($this->_con);
			unset($_SESSION['score_id']);
			unset($_SESSION['stopwatch']);
			unset($_SESSION['is_correct']);
			unset($_SESSION['answers']);
			return $results;
		}else{
			$results = array();
			$results['inleaders'] = $this->checkLeaders();
			return $results;
		}	
	}
	
	public function checkLeaders(){
		$query ="SELECT * FROM (SELECT user_id, score FROM scores WHERE score != 0 ORDER BY user_id, score ASC ) AS x GROUP BY user_id ORDER BY x.score ASC LIMIT 5";
		$result = mysqli_query( $this->_con, $query);
		$leaders = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$leaders[] += $row['user_id'];
		}
		
	return in_array($_SESSION[id],$leaders);
	}
	
	
}