<?php 
ob_start();
session_start();
require_once 'config.php'; 

if( !empty( $_POST ) && !isset($_SESSION['logged_in'])){
	try {
		$user_obj = new Cl_User();
		$data = $user_obj->login( $_POST );
	} catch (Exception $e) {
		$error = $e->getMessage();
		$_SESSION['error'] = $error;
	}
}
require_once 'templates/header.php';
require_once 'templates/message.php';
?>
<?php if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){ ?>
<div class="content">
     	<div class="game-container">
			<a class="logo as-supersign"><img src="img/logo-white.svg"></a>
				<h1 id="title" class="color-white">Ожидание...</h1>
					<p id="comment" class="color-white">Немного терпения, мы ждём остальных игроков</p>
		</div>
		<div class="bounces">
			<div class="bounce-left"></div>
			<div class="bounce-middle">
				<div class="preloader"><div></div><div></div><div></div><div></div></div>
				<a id="start_btn" class="bounce-start hide" href="/questions">Начать игру</a>
				<a id="winner_btn" class="bounce-start hide" href="/winners">Итоги игры</a>
			</div>
			<div class="bounce-right"></div>
		</div>
     	<div class="game-container">
					<p class="color-white text-small">Твой номер телефона: <?php echo "+".$_SESSION['phone']; ?> | <a href="logout">Выйти</a><br/>
					Будь готов подтвердить его при получении приза</p>
     	</div>
</div>
	
<script>
poll();
repeater = setInterval(poll, 2000);
function poll() {
	$.ajax({
		url: '/server.php', type: 'GET', dataType: 'json', 
		data: { check : 'status'},
		cache: false,
		headers: {
              "cache-control": "no-cache"
            },
		success: function(){
		},
		complete: function(tet) {
			var answer =  tet.responseJSON;
			if (answer == 'RUN') {
				clearInterval(repeater);
				$(".preloader").fadeOut();
				$("#title").fadeOut(function() {
					$(this).text("Готово!");
					$('#start_btn').fadeIn('slow');
				}).fadeIn();
				$("#comment").fadeOut(function() {
					$(this).text("Нажми на кнопку «Начать игру» и отвечай на вопросы!")
				}).fadeIn();
			
			}
			else if (answer == 'GAMEOVER'){
				$(".preloader").fadeOut();
				$("#title").fadeOut(function() {
					$(this).text("Игра закончилась");
					$('#winner_btn').fadeIn('slow');
				}).fadeIn();
				$("#comment").fadeOut(function() {
					$(this).text("Не забудь посмотреть, выиграл ли ты приз")
				}).fadeIn();				
				clearInterval(repeater);
			}
        }
	})
};

</script>	

	<?php require_once 'templates/footer.php';
}
	//Регистрация
else{ ?>

	<div class="content bg-pic1 pt-50">
	<div class="login-container">
		<a class="logo as-supersign"><img src="img/logo-white.svg"></a>
		<div class="login-form">
          <h1 class="color-white">Готов получать<br> подарки?</h1>
			<h2 class="color-mfpurple">Пройди квиз быстрее всех и получи подарок от МегаФона!</h2>
			<form id="login-form" method="post" class="form-signin" role="form" action="./">
				<input name="phone" id="phone" type="tel" class="login-phone" placeholder="+7 ( ___ ) ___-__-__"> 
				<p class="color-white text-muted text-small">Введи номер телефона, чтобы начать игру. <br>Мы не храним эти данные и обещаем не рассылать спам.</p>
				<button class="btn btn-mfpurple btn-login" type="submit">Начать</button>
			</form>
		</div>
	</div>
	</div>
	<!-- /container -->
    <script src="js/login.js"></script>
<?php require_once 'templates/footer.php';?>
<?php }?>    