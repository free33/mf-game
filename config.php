<?php

require_once 'user.php';

define('LOGIN_FIELDS_MISSING', 'Введите номер телефона');
define('GAMEOVER', 'Игра закончилась');
define('BUYEARLYACCESS', 'Игра ещё не началась');
define('LOGIN_USER_AUTHORIZED', 'Пользователь с таким номером уже авторизован');
define('LOGOUT_SUCCESS', 'Вы вышли из игры');

define( 'DB_HOST', '' );
define( 'DB_USERNAME', '');
define( 'DB_PASSWORD', '');
define( 'DB_NAME', '');

define( 'QUIZ_TIMER', 12); 
define( 'QUIZ_COUNT_WINNERS', 5); 

