<?php 
ob_start();
session_start();
require_once 'config.php'; 
if(!isset($_SESSION['logged_in'])){
	header('Location: /');
	exit;	
}
try {
	$user = new Cl_User();
	$result = $user->checkLeaders();
			switch ($user->gameStatus()){
			case 'WAIT' :  throw new Exception( BUYEARLYACCESS ); break;	
		}
	} catch (Exception $e) {
		$_SESSION['error'] = $e->getMessage();
		header('Location: /');exit;
	} 
?>
<?php require_once 'templates/header.php';?>
<?php if ($result){ ?>
<div class="content">
     	<div class="game-container">
			<a class="logo as-supersign"><img src="img/logo-white.svg"></a>
				<h1 class="color-white">Поздравляем!</h1>
					<p class="color-white">Ты быстрее всех остальных верно ответил на вопросы!</p>
					<p class="color-white">Покажи это сообщение промоутеру, чтобы получить заслуженный приз</p>
		</div>
		<div class="bounces">
			<div class="bounce-left"></div>
			<div class="bounce-middle">
				<div class="prize"></div>
			</div>
			<div class="bounce-right"></div>
		</div>
     	<div class="game-container">
					<p class="color-white text-small">Твой номер телефона: <?php echo "+".$_SESSION['phone']; ?> | <a href="logout">Выйти</a>
					<br/>
					Будь готов подтвердить его при получении приза</p>
     	</div>
</div>
<?php } else {?>
	<div class="content bg-pic1 pt-50">
		<div class="game-container">
			<a class="logo as-supersign"><img src="img/logo-white.svg"></a>
				<h1 id="title" id="notintop" class="color-white">Упс :(</h1>
				<p class="color-white">Кто-то оказался быстрее и точнее тебя. Не отчаивайся, повезёт в следующий раз</p>
				<p class="color-white">&#3900; &#12388; &#9685;_&#9685; &#3901;&#12388;</p>
     	</div>
     	<div class="game-container">
					<p class="color-white text-small">Твой номер телефона: <?php echo "+".$_SESSION['phone']; ?> | <a href="logout">Выйти</a></p>
     	</div>
    </div>
<?php }?>
<?php require_once 'templates/footer.php';?>